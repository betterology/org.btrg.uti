package com.foo.bar;

import org.btrg.uti.IUtia;

/**
 * example of code that might call IUtia
 */
@Component
public class MyClient {
	private IUtia utia;
	
	@Autowired
	MyClient(IUtia utia){
		this.utia = utia;
	}
	
}
