package org.btrg.utils.date;

import java.util.Date;

import org.btrg.uti.DateUtils_;
import org.btrg.uti.StringUtils_;

//import com.sun.org.apache.xml.internal.serializer.ToUnknownStream;

import junit.framework.Assert;
import junit.framework.TestCase;

public class DateUtilsTest extends TestCase {

	public void testToDateFromYyyyMMdd() {
		Date date = DateUtils_.getDateFromYyyyMMdd("20100101");
		assertEquals("Fri Jan 01 00:00:00", date.toString().substring(0,19));
	}

	public void testFollowingDay() {
		String dateStartString = "2008-01-01";
		String dateEndString = "2008-01-13";
		Date dateStart = DateUtils_.toUtilDate(dateStartString);
		Date dateEnd = DateUtils_.toUtilDate(dateEndString);
		assertEquals(StringUtils_.yyyyMMdd(dateEnd),
				StringUtils_.yyyyMMdd(DateUtils_.followingDay(dateStart, 12)));
	}

	public void testTimeDifference() {
		String timeStart = StringUtils_.yyyyMMddHHmmss(new Date());
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		assertTrue(10 == DateUtils_.dateSecondsDifference(timeStart));
	}

	public void testPrefixedYyMMdd() {
		String testMe = "110920.txt";
		Date testDate = DateUtils_.prefixedWithYyMMdd(testMe);
		assertEquals("Tue Sep 20 07:00:00", testDate.toString().substring(0,19));
	}

}
