package org.btrg.utils.flags;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import org.btrg.uti.BitwiseFlag_;
import org.btrg.uti.internal.flags.SampleIntFlag;

/**
 * 
 * @author petecarapetyan
 * 
 */
public class BitwiseFlagUnderstandingTest implements SampleIntFlag {

	@Test
	public void testHas() {
		int a = INT_1;
		int b = INT_2;
		int c = INT_4;
		int flags = a | b;
		assertTrue(BitwiseFlag_.hasFlag_(flags, b));
		assertTrue(BitwiseFlag_.hasFlag_(flags, a));
	}

	@Test
	public void testDoesntHave() {
		int a = INT_1;
		int b = INT_2;
		int c = INT_4;
		int flags = a | b;
		assertFalse(BitwiseFlag_.hasFlag_(flags, c));
	}

	@Test
	public void testTonOfStuffStillWorks() {
		int a = INT_1;
		int b = INT_2;
		int c = INT_4;
		int d = INT_1024;
		int e = INT_1048576;
		int f = INT_128;
		int g = INT_131072;
		int h = INT_134217728;
		int j = INT_2048;
		int k = INT_536870912;
		int l = INT_64;
		int flags = a | b | c | d | e;
		assertTrue(BitwiseFlag_.hasFlag_(flags, a));
		assertTrue(BitwiseFlag_.hasFlag_(flags, b));
		assertTrue(BitwiseFlag_.hasFlag_(flags, c));
		assertTrue(BitwiseFlag_.hasFlag_(flags, d));
		assertTrue(BitwiseFlag_.hasFlag_(flags, e));
		assertFalse(BitwiseFlag_.hasFlag_(flags, f));
		assertFalse(BitwiseFlag_.hasFlag_(flags, g));
		assertFalse(BitwiseFlag_.hasFlag_(flags, f));
		assertFalse(BitwiseFlag_.hasFlag_(flags, h));
		assertFalse(BitwiseFlag_.hasFlag_(flags, j));
		assertFalse(BitwiseFlag_.hasFlag_(flags, k));
		assertFalse(BitwiseFlag_.hasFlag_(flags, l));
	}

	@Test
	public void testWoopsAddedSameThingMultipleTimes() {
		int a = INT_1;
		int b = INT_2;
		int c = INT_4;
		int d = INT_1024;
		int e = INT_1048576;
		int f = INT_128;
		int g = INT_131072;
		int h = INT_134217728;
		int j = INT_2048;
		int k = INT_536870912;
		int l = INT_64;
		int flags = a | b | c | d | e | a | b | c | d | e | b | c | d | a | b
				| c | d | e;
		assertTrue(BitwiseFlag_.hasFlag_(flags, a));
		assertTrue(BitwiseFlag_.hasFlag_(flags, b));
		assertTrue(BitwiseFlag_.hasFlag_(flags, c));
		assertTrue(BitwiseFlag_.hasFlag_(flags, d));
		assertTrue(BitwiseFlag_.hasFlag_(flags, e));
		assertFalse(BitwiseFlag_.hasFlag_(flags, f));
		assertFalse(BitwiseFlag_.hasFlag_(flags, g));
		assertFalse(BitwiseFlag_.hasFlag_(flags, f));
		assertFalse(BitwiseFlag_.hasFlag_(flags, h));
		assertFalse(BitwiseFlag_.hasFlag_(flags, j));
		assertFalse(BitwiseFlag_.hasFlag_(flags, k));
		assertFalse(BitwiseFlag_.hasFlag_(flags, l));
	}

	@Test
	public void testRemoveFlag() {
		int a = INT_1;
		int b = INT_2;
		int c = INT_4;
		int flags = a | b;
		assertTrue(BitwiseFlag_.hasFlag_(flags, b));
		assertTrue(BitwiseFlag_.hasFlag_(flags, a));
		flags = BitwiseFlag_.toggleFlag_(flags, b);
		assertFalse(BitwiseFlag_.hasFlag_(flags, b));
		flags = BitwiseFlag_.toggleFlag_(flags, b);
		assertTrue(BitwiseFlag_.hasFlag_(flags, b));
		flags = BitwiseFlag_.toggleFlag_(flags, INT_536870912);
		assertTrue(BitwiseFlag_.hasFlag_(flags, INT_536870912));
	}

	@Test
	public void testToggleStateToOnOrOffRegardlessOfCurrentState() {
		int a = INT_1;
		int b = INT_2;
		int c = INT_4;
		int f = INT_128;
		int g = INT_131072;
		int h = INT_134217728;
		int j = INT_2048;
		int k = INT_536870912;
		int l = INT_64;
		int flags = a | b | f | g | h | k;
		flags = BitwiseFlag_.toggleFlag_(flags, b);
		flags = BitwiseFlag_.toggleFlag_(flags, b);
		flags = BitwiseFlag_.toggleFlag_(flags, j);
		assertTrue(BitwiseFlag_.hasFlag_(flags, k));
		flags = BitwiseFlag_.toggleFlag_(flags, k);
		assertFalse(BitwiseFlag_.hasFlag_(flags, k));
		flags = BitwiseFlag_.toggleFlagToOnRegardlessOfCurrentState_(flags, k);
		assertTrue(BitwiseFlag_.hasFlag_(flags, k));
		flags = BitwiseFlag_.toggleFlagToOffRegardlessOfCurrentState_(flags, k);
		assertFalse(BitwiseFlag_.hasFlag_(flags, k));
	}

	@Test
	public void testStringValues() {
		assertEquals(
				BitwiseFlag_.integerAsBinaryShowingAllZeroesToString_(INT_1),
				"00000000000000000000000000000001");
		assertEquals(
				BitwiseFlag_.integerAsBinaryShowingAllZeroesToString_(INT_2),
				"00000000000000000000000000000010");
		assertEquals(
				BitwiseFlag_.integerAsBinaryShowingAllZeroesToString_(INT_4),
				"00000000000000000000000000000100");
		assertEquals(
				BitwiseFlag_.integerAsBinaryShowingAllZeroesToString_(INT_8),
				"00000000000000000000000000001000");
		assertEquals(
				BitwiseFlag_.integerAsBinaryShowingAllZeroesToString_(INT_16),
				"00000000000000000000000000010000");
		assertEquals(
				BitwiseFlag_.integerAsBinaryShowingAllZeroesToString_(INT_32),
				"00000000000000000000000000100000");
		assertEquals(
				BitwiseFlag_.integerAsBinaryShowingAllZeroesToString_(INT_64),
				"00000000000000000000000001000000");
		assertEquals(
				BitwiseFlag_.integerAsBinaryShowingAllZeroesToString_(INT_128),
				"00000000000000000000000010000000");
		assertEquals(
				BitwiseFlag_.integerAsBinaryShowingAllZeroesToString_(INT_256),
				"00000000000000000000000100000000");
		assertEquals(
				BitwiseFlag_.integerAsBinaryShowingAllZeroesToString_(INT_512),
				"00000000000000000000001000000000");
		assertEquals(
				BitwiseFlag_.integerAsBinaryShowingAllZeroesToString_(INT_1024),
				"00000000000000000000010000000000");
		assertEquals(
				BitwiseFlag_.integerAsBinaryShowingAllZeroesToString_(INT_2048),
				"00000000000000000000100000000000");
		assertEquals(
				BitwiseFlag_.integerAsBinaryShowingAllZeroesToString_(INT_4096),
				"00000000000000000001000000000000");
		assertEquals(
				BitwiseFlag_.integerAsBinaryShowingAllZeroesToString_(INT_8192),
				"00000000000000000010000000000000");
		assertEquals(
				BitwiseFlag_
						.integerAsBinaryShowingAllZeroesToString_(INT_16384),
				"00000000000000000100000000000000");
		assertEquals(
				BitwiseFlag_
						.integerAsBinaryShowingAllZeroesToString_(INT_32768),
				"00000000000000001000000000000000");
		assertEquals(
				BitwiseFlag_
						.integerAsBinaryShowingAllZeroesToString_(INT_65536),
				"00000000000000010000000000000000");
		assertEquals(
				BitwiseFlag_
						.integerAsBinaryShowingAllZeroesToString_(INT_131072),
				"00000000000000100000000000000000");
		assertEquals(
				BitwiseFlag_
						.integerAsBinaryShowingAllZeroesToString_(INT_262144),
				"00000000000001000000000000000000");
		assertEquals(
				BitwiseFlag_
						.integerAsBinaryShowingAllZeroesToString_(INT_524288),
				"00000000000010000000000000000000");
		assertEquals(
				BitwiseFlag_
						.integerAsBinaryShowingAllZeroesToString_(INT_1048576),
				"00000000000100000000000000000000");
		assertEquals(
				BitwiseFlag_
						.integerAsBinaryShowingAllZeroesToString_(INT_2097152),
				"00000000001000000000000000000000");
		assertEquals(
				BitwiseFlag_
						.integerAsBinaryShowingAllZeroesToString_(INT_4194304),
				"00000000010000000000000000000000");
		assertEquals(
				BitwiseFlag_
						.integerAsBinaryShowingAllZeroesToString_(INT_8388608),
				"00000000100000000000000000000000");
		assertEquals(
				BitwiseFlag_
						.integerAsBinaryShowingAllZeroesToString_(INT_16777216),
				"00000001000000000000000000000000");
		assertEquals(
				BitwiseFlag_
						.integerAsBinaryShowingAllZeroesToString_(INT_33554432),
				"00000010000000000000000000000000");
		assertEquals(
				BitwiseFlag_
						.integerAsBinaryShowingAllZeroesToString_(INT_67108864),
				"00000100000000000000000000000000");
		assertEquals(
				BitwiseFlag_
						.integerAsBinaryShowingAllZeroesToString_(INT_134217728),
				"00001000000000000000000000000000");
		assertEquals(
				BitwiseFlag_
						.integerAsBinaryShowingAllZeroesToString_(INT_268435456),
				"00010000000000000000000000000000");
		assertEquals(
				BitwiseFlag_
						.integerAsBinaryShowingAllZeroesToString_(INT_536870912),
				"00100000000000000000000000000000");
		assertEquals(
				BitwiseFlag_
						.integerAsBinaryShowingAllZeroesToString_(INT_1073741824),
				"01000000000000000000000000000000");
	}

	@Test
	public void testPrint() {
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(INT_1);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(INT_2);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(INT_4);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(INT_8);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(INT_16);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(INT_32);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(INT_64);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(INT_128);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(INT_256);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(INT_512);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(INT_1024);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(INT_2048);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(INT_4096);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(INT_8192);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(INT_16384);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(INT_32768);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(INT_65536);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(INT_131072);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(INT_262144);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(INT_524288);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(INT_1048576);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(INT_2097152);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(INT_4194304);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(INT_8388608);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(INT_16777216);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(INT_33554432);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(INT_67108864);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(INT_134217728);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(INT_268435456);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(INT_536870912);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(INT_1073741824);
	}

	@Test
	public void testShiftOperator() {
//		System.print.println("\n\n1 << n start");
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(1 << 0);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(1 << 1);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(1 << 2);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(1 << 3);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(1 << 4);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(1 << 5);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(1 << 6);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(1 << 7);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(1 << 8);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(1 << 9);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(1 << 10);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(1 << 11);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(1 << 12);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(1 << 13);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(1 << 14);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(1 << 15);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(1 << 16);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(1 << 17);
		BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_(1 << 18);
//		System.print.println("1 << n finish\n\n");
	}

	@Test
	public void testMultipleFlagsCombination() {
		int a = INT_1;
		int b = INT_2;
		int c = INT_4;
		int d = INT_1024;
		int e = INT_1048576;
		int f = INT_128;
		int g = INT_131072;
		int h = INT_134217728;
		int j = INT_2048;
		int k = INT_536870912;
		int l = INT_64;
		int fooTest1 = a | d;
		int fooTest2 = a | e;
		int fooTest3 = a | k;
		int barTest1 = b | d;
		int barTest2 = b | e;
		int barTest3 = b | k;
		int flags1 = a | d | e | f | g;
		int flags2 = b | d | e | f | g;
		assertTrue(BitwiseFlag_.hasFlag_(flags1, fooTest1));
		assertFalse(BitwiseFlag_.hasFlag_(flags1, barTest1));
		assertTrue(BitwiseFlag_.hasFlag_(flags1, fooTest2));
		assertFalse(BitwiseFlag_.hasFlag_(flags1, barTest2));
		assertFalse(BitwiseFlag_.hasFlag_(flags2, fooTest1));
		assertTrue(BitwiseFlag_.hasFlag_(flags2, barTest1));
		assertFalse(BitwiseFlag_.hasFlag_(flags2, fooTest2));
		assertTrue(BitwiseFlag_.hasFlag_(flags2, barTest2));
		assertFalse(BitwiseFlag_.hasFlag_(flags1, barTest3));
		assertFalse(BitwiseFlag_.hasFlag_(flags2, barTest3));
		assertFalse(BitwiseFlag_.hasFlag_(flags1, fooTest3));
		assertFalse(BitwiseFlag_.hasFlag_(flags2, fooTest3));
	}
}
