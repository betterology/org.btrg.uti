package org.btrg.utils.random;

import static org.junit.Assert.*;

import org.btrg.uti.RandomIntGenerator_;
import org.junit.Test;

public class RandomParagraphTest {

	@Test
	public void test() {
		for(int i = 0;i<10;i++){
			String paragraph = RandomParagraph.getParagraph(RandomIntGenerator_.get(5));
			assertTrue(paragraph.length()>0);
		}
	}

}
