package org.btrg.utils.random;


import static org.junit.Assert.*;

import org.junit.Test;

public class RandomProviderTest {

	@Test
	public void testRandomName() {
		assertTrue(RandomProvider.randomName().contains(" "));
		assertTrue(RandomProvider.randomName().length() > 8);
	}

	@Test
	public void testWeightedToYesBoolean() {
		int count1 = 0;
		boolean comparison = true;
		for (int i = 1; i < 1000; i++) {
			comparison = RandomProvider.weightedToYesBoolean(90);
			if (comparison) {
				count1++;
			}
		}
		assertTrue(count1 > 850 && count1 < 950);
	}

	@Test
	public void testWeighted1or2or3() {
		int count1 = 0;
		int count2 = 0;
		int count3 = 0;
		int comparison = 0;
		for (int i = 1; i < 1000; i++) {
			comparison = RandomProvider.weighted1or2or3(80, 15, 5);
			if (comparison == 1) {
				count1++;
			}
			if (comparison == 2) {
				count2++;
			}
			if (comparison == 3) {
				count3++;
			}
		}
		assertTrue(count1 > 700 && count1 < 900);
		assertTrue(count2 > 100 && count2 < 200);
		assertTrue(count3 > 20 && count3 < 100);
	}

	@Test
	public void testWeightedGroup() {
		int count1 = 0;
		int count2 = 0;
		int count3 = 0;
		int count4 = 0;
		String comparison = "Bravo";
		for (int i = 1; i < 1000; i++) {
			comparison = RandomProvider.weightedGroup(60, 20, 15, 5);
			if (comparison.equals("Bravo")) {
				count1++;
			}
			if (comparison.equals("Tango")) {
				count2++;
			}
			if (comparison.equals("Alpha")) {
				count3++;
			}
			if (comparison.equals("Delta")) {
				count4++;
			}
		}
		assertTrue(count1 > 500 && count1 < 700);
		assertTrue(count2 > 150 && count2 < 250);
		assertTrue(count3 > 100 && count3 < 200);
		assertTrue(count4 > 10 && count4 < 90);
	}

	@Test
	public void testRandom1toMax() {
		int max = -1;
		int min = Integer.MAX_VALUE;
		int test = 0;
		for (int i = 0; i < 100; i++) {
			test = RandomProvider.random1toMax(10);
			if (min > test) {
				min = test;
			}
			if (max < test) {
				max = test;
			}
		}
		assertTrue(min==1);
		assertTrue(max==10);
	}

	@Test
	public void testRandom0toMax() {
		int max = -1;
		int min = Integer.MAX_VALUE;
		int test = 0;
		for (int i = 0; i < 1000; i++) {
			test = RandomProvider.random0toMax(7);
			if (min > test) {
				min = test;
			}
			if (max < test) {
				max = test;
			}
		}
		assertTrue(min==0);
		assertTrue(max==7);
		for (int i = 0; i < 10000; i++) {
			test = RandomProvider.random0toMax(32);
			if (min > test) {
				min = test;
			}
			if (max < test) {
				max = test;
			}
		}
		assertTrue(min==0);
		assertTrue(max==32);
	}

}
