package org.btrg.utils.file;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.btrg.uti.FileAsString_;

import junit.framework.TestCase;

public class FileAsStringTest extends TestCase {

	public void testToStringFile() {

		String messageFile = null;
		try {
			messageFile = FileAsString_.read(new File("testFile.txt"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals("foo", messageFile);
	}
	


	public void testToStringString() {

		String messageFile = null;
		try {
			messageFile = FileAsString_.read("testFile.txt");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals("foo", messageFile);
	}

}
