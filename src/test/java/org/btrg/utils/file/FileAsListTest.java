package org.btrg.utils.file;

import java.io.File;
import java.util.List;

import junit.framework.TestCase;

import org.btrg.uti.FileAsList_;

public class FileAsListTest extends TestCase {

	public void testToStringString() {
		// for some reason I have to do this indirectly rather than the way it
		// is done in the real app with getResource()
		List lines = FileAsList_.read(new File(
				"src/test/java/org/btrg/utils/file/one.txt"));
		assertEquals(16, lines.size());
	}

}
