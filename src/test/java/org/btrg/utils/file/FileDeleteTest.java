package org.btrg.utils.file;

import static org.junit.Assert.*;

import java.io.File;

import org.btrg.uti.FileDelete;
import org.btrg.uti.SeedFile_;
import org.junit.Test;

public class FileDeleteTest {

	@Test
	public void test() {
		SeedFile_.instance().seed("hi.txt", "hi");
		File file = new File("hi.txt");
		if(!file.exists()){
			fail("woops");
		}
		FileDelete.delete("hi.txt");
		if(file.exists()){
			fail("woops");
		}
	}

}
