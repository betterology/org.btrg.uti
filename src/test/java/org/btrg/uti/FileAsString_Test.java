package org.btrg.uti;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.btrg.uti.*;
import org.junit.Test;

public class FileAsString_Test extends AbstractUtiTest {

	@Test
	public void testRead() {
		File file = new File("./src/test/resources/docInput.txt");
		assertTrue(file.exists());
		try {
			assertEquals(147473, FileAsString_.read(file).length());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			fail();
		} catch (IOException e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	public void testRead2() {
		try {
			assertEquals(147473, FileAsString_.read("./src/test/resources/docInput.txt").length());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			fail();
		} catch (IOException e) {
			e.printStackTrace();
			fail();
		}
	}
}

// import static org.junit.Assert.*;
// import static org.mockito.Mockito.*;
// @BeforeClass public static void setUpBeforeClass() throws Exception {}
// @AfterClass public static void tearDownAfterClass() throws Exception {}
// @Before public void setUp() throws Exception {}
// @After public void tearDown() throws Exception {}
