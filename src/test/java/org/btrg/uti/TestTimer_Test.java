package org.btrg.uti;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import org.btrg.uti.*;
import org.junit.Test;

public class TestTimer_Test extends AbstractUtiTest {


    @Test
    public void testReset(){
        log.incompleteTestLowPriority(this, "TestTimer_.reset()");
    }

    @Test
    public void testReset2(){
        log.incompleteTestLowPriority(this, "TestTimer_.reset()");
    }

    @Test
    public void testDifference(){
        log.incompleteTestLowPriority(this, "TestTimer_.difference()");
    }

    @Test
    public void testDoneMinutes(){
        log.incompleteTestLowPriority(this, "TestTimer_.doneMinutes()");
    }

    @Test
    public void testDoneSeconds(){
        log.incompleteTestLowPriority(this, "TestTimer_.doneSeconds()");
    }

    @Test
    public void testElapsedMinutes(){
        log.incompleteTestLowPriority(this, "TestTimer_.elapsedMinutes()");
    }

    @Test
    public void testElapsedSeconds(){
        log.incompleteTestLowPriority(this, "TestTimer_.elapsedSeconds()");
    }

    @Test
    public void testDoneSecondsMessage(){
        log.incompleteTestLowPriority(this, "TestTimer_.doneSecondsMessage()");
    }

    @Test
    public void testDoneMinutesMessage(){
        log.incompleteTestLowPriority(this, "TestTimer_.doneMinutesMessage()");
    }
}


// import static org.junit.Assert.*;
// import static org.mockito.Mockito.*;
// @BeforeClass public static void setUpBeforeClass() throws Exception {}
// @AfterClass public static void tearDownAfterClass() throws Exception {}
// @Before public void setUp() throws Exception {}
// @After public void tearDown() throws Exception {}
