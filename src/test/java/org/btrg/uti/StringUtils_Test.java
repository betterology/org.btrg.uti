package org.btrg.uti;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import org.btrg.uti.*;
import org.junit.Test;

public class StringUtils_Test extends AbstractUtiTest {

	// mixed between old and new tests please edit carefully

	private final String EVERY_CHAR_ON_KEYBOARD = " !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~";
	private final String EVERY_CHAR_SHIFTED_6 = "&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~ !\"#$";

	@Test
	public void testCamelCaseConvertToConstant(){
		assertEquals("0", StringUtils_.camelCaseConvertToConstant("0"));
		assertEquals("9", StringUtils_.camelCaseConvertToConstant("9"));
		assertEquals("A", StringUtils_.camelCaseConvertToConstant("A"));
		assertEquals("Z", StringUtils_.camelCaseConvertToConstant("Z"));
		assertEquals("A", StringUtils_.camelCaseConvertToConstant("a"));
		assertEquals("Z", StringUtils_.camelCaseConvertToConstant("z"));
		assertEquals("APPLE_SAUCE", StringUtils_.camelCaseConvertToConstant("appleSauce"));
		assertEquals("APPLE_SAUCE", StringUtils_.camelCaseConvertToConstant("AppleSauce"));
	}
	
	private static String getFullBreadthAll95charsString() {
		String returnValue = "";
		byte[] fullBreadthByte = new byte[95];
		for (int i = 32; i < fullBreadthByte.length + 32; i++) {
			fullBreadthByte[i - 32] = (byte) i;
			// System.print.println("" + i + " = " + fullBreadthByte[i]);
		}
		returnValue = new String(fullBreadthByte);
		return returnValue;
	}

	@Test
	public void testReplace() {
		log.incompleteTestBecause_(this, "StringUtils_.replace()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testLoadConvert() {
		log.incompleteTestBecause_(this, "StringUtils_.loadConvert()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testYyMMdd() {
		log.incompleteTestBecause_(this, "StringUtils_.yyMMdd()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testGetFrontFilledFixedLengthString() {
		String incoming = "foo";
		String outgoing = "___foo";
		assertEquals(outgoing, StringUtils_.getFrontFilledFixedLengthString(
				incoming, '_', 6, false, false));
	}

	@Test
	public void testDotPathToSlashPath() {
		log.incompleteTestInactivePart(this,
				"StringUtils_.dotPathToSlashPath()");
	}

	@Test
	public void testTimeFileName() {
		log.incompleteTestInactivePart(this, "StringUtils_.timeFileName()");
	}

	@Test
	public void testPrintProperties() {
		log.incompleteTestInactivePart(this, "StringUtils_.printProperties()");
	}

	@Test
	public void testAssertNotBlank() {
		log.incompleteTestInactivePart(this, "StringUtils_.assertNotBlank()");
	}

	@Test
	public void testNotNull() {
		log.incompleteTestInactivePart(this, "StringUtils_.notNull()");
	}

	@Test
	public void testLastToken() {
		log.incompleteTestInactivePart(this, "StringUtils_.lastToken()");
	}

	@Test
	public void testGetClassNameLikeFromDelimited() {
		log.incompleteTestInactivePart(this,
				"StringUtils_.getClassNameLikeFromDelimited()");
	}

	@Test
	public void testGetMethodNameLikeFromDelimited() {
		log.incompleteTestInactivePart(this,
				"StringUtils_.getMethodNameLikeFromDelimited()");
	}

	@Test
	public void testNumberToLetter() {
		log.incompleteTestInactivePart(this, "StringUtils_.numberToLetter()");
	}

	@Test
	public void testUpLow() {
		String myFooBar = "myFooBar";
		String expectedMyFooBar = "Myfoobar";
		assertEquals(expectedMyFooBar, StringUtils_.upLow(myFooBar));
	}

	@Test
	public void testUpStart() {
		log.incompleteTestBecause_(this, "StringUtils_.upStart()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testLowStart() {
		log.incompleteTestBecause_(this, "StringUtils_.lowStart()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testTrimJavaPeteCStyle() {
		log.incompleteTestBecause_(this, "StringUtils_.trimJavaPeteCStyle()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testIsYesNo() {
		log.incompleteTestBecause_(this, "StringUtils_.isYesNo()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testIsYesNo2() {
		log.incompleteTestBecause_(this, "StringUtils_.isYesNo()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testTrueTest() {
		log.incompleteTestBecause_(this, "StringUtils_.trueTest()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testYesNoToTrueFalse() {
		log.incompleteTestBecause_(this, "StringUtils_.yesNoToTrueFalse()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testListFromDelimited() {
		log.incompleteTestBecause_(this, "StringUtils_.listFromDelimited()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testFalseTest() {
		log.incompleteTestBecause_(this, "StringUtils_.falseTest()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testBooleanStringReturn() {
		log.incompleteTestBecause_(this, "StringUtils_.booleanStringReturn()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testIsNull() {
		log.incompleteTestBecause_(this, "StringUtils_.isNull()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testSplitStringList() {
		log.incompleteTestBecause_(this, "StringUtils_.splitStringList()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testSplitStringListDelimiter() {
		log.incompleteTestBecause_(this, "StringUtils_.splitStringListDelimiter()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testSplitString() {
		log.incompleteTestBecause_(this, "StringUtils_.splitString()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testSplitStringWithDelimiter() {
		log.incompleteTestBecause_(this, "StringUtils_.splitStringWithDelimiter()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testSplitStringForcedDelimiter() {
		log.incompleteTestBecause_(this, "StringUtils_.splitStringForcedDelimiter()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testNuProjectHash() {
		log.incompleteTestBecause_(this, "StringUtils_.nuProjectHash()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testRandomString() {
		log.incompleteTestBecause_(this, "StringUtils_.randomString()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testUsername() {
		log.incompleteTestBecause_(this, "StringUtils_.username()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testRandomNumberString() {
		log.incompleteTestBecause_(this, "StringUtils_.randomNumberString()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testOkNumber() {
		log.incompleteTestBecause_(this, "StringUtils_.okNumber()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testSomeLower() {
		log.incompleteTestBecause_(this, "StringUtils_.someLower()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testNumberOnly() {
		log.incompleteTestBecause_(this, "StringUtils_.numberOnly()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testDatePrint() {
		log.incompleteTestBecause_(this, "StringUtils_.datePrint()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testYyMMddHHmmssPrint() {
		log.incompleteTestBecause_(this, "StringUtils_.yyMMddHHmmssPrint()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testYyyy_MM_dd() {
		log.incompleteTestBecause_(this, "StringUtils_.yyyy_MM_dd()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testDayMonthYear() {
		log.incompleteTestBecause_(this, "StringUtils_.dayMonthYear()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testYyyyDashMMDashdd() {
		log.incompleteTestBecause_(this, "StringUtils_.yyyyDashMMDashdd()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testGetDd_MMM_yyDate() {
		log.incompleteTestBecause_(this, "StringUtils_.getDd_MMM_yyDate()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testDd_MMM_yy() {
		log.incompleteTestBecause_(this, "StringUtils_.dd_MMM_yy()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testYyyyMMdd() {
		log.incompleteTestBecause_(this, "StringUtils_.yyyyMMdd()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testYyMMddHHmmss() {
		log.incompleteTestBecause_(this, "StringUtils_.yyMMddHHmmss()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testMMddyyyy() {
		log.incompleteTestBecause_(this, "StringUtils_.MMddyyyy()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testYyyyMMddHHmmss() {
		log.incompleteTestBecause_(this, "StringUtils_.yyyyMMddHHmmss()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testEndSingleSlash() {
		log.incompleteTestBecause_(this, "StringUtils_.endSingleSlash()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testRemoveLineHavingValue() {
		log.incompleteTestBecause_(this, "StringUtils_.removeLineHavingValue()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testRemoveEmptyLines() {
		log.incompleteTestBecause_(this, "StringUtils_.removeEmptyLines()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testOrderLinesByAlpha() {
		log.incompleteTestBecause_(this, "StringUtils_.orderLinesByAlpha()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testAsciiIsInTextRange() {
		log.incompleteTestBecause_(this, "StringUtils_.asciiIsInTextRange()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testShift6() {
		String fullBreadthChars = getFullBreadthAll95charsString();
		assertTrue(EVERY_CHAR_ON_KEYBOARD.equals(fullBreadthChars));
		String convertedByShifting6 = StringUtils_
				.shift6(EVERY_CHAR_ON_KEYBOARD);
		// System.print.println("'" + convertedByShifting6 + "'");
		assertEquals(convertedByShifting6, EVERY_CHAR_SHIFTED_6);
		// System.print.println("'" + fullBreadthChars + "'");

	}

	@Test
	public void testShift6back() {
		// fails because does not know how to handle /n or /t for example, and
		// I'm too lazy to figure out how to fix it because it would be faster
		// to figure out how to use regular encryption
		String stringToTest = EVERY_CHAR_ON_KEYBOARD + "\n"
				+ EVERY_CHAR_ON_KEYBOARD + "\n" + EVERY_CHAR_ON_KEYBOARD;
		String convertedStringToTest = StringUtils_.shift6(stringToTest);
		// System.print.println(StringUtils_.shift6back(convertedStringToTest));
		// System.print.println(stringToTest);
		assertEquals(stringToTest,
				StringUtils_.shift6back(convertedStringToTest));

	}

	@Test
	public void testSubtract6() {
		log.incompleteTestBecause_(this, "StringUtils_.subtract6()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testAdd6() {
		log.incompleteTestBecause_(this, "StringUtils_.add6()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testGetPaddedString() {
		String testAgainstThis = "fullOn        ";
		String startWith = "fullOn";
		assertEquals(testAgainstThis,
				StringUtils_.getPaddedString(startWith, 14, ' ', false));
		testAgainstThis = "fullOnToMe  going to town with this one";
		try {
			StringUtils_.getPaddedString(testAgainstThis, 14, ' ', false);
			fail("woops should not have got this far");
		} catch (IllegalArgumentException e) {

		}

	}

	@Test
	public void testGetPrePaddedString() {
		log.incompleteTestBecause_(this, "StringUtils_.getPrePaddedString()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testRemoveEndPaddingFromString() {
		String startingString = "foo________________";
		String startingString2 = "foobeat           ";
		assertEquals("foo",
				StringUtils_.removeEndPaddingFromString(startingString, '_'));
		assertEquals("foobeat",
				StringUtils_.removeEndPaddingFromString(startingString2, ' '));

	}

	@Test
	public void testRemoveMultipleWhiteSpaceCharachters() {
		log.incompleteTestBecause_(this,
				"StringUtils_.removeMultipleWhiteSpaceCharachters()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testIsWhiteSpace() {
		String startingString = "foo_";
		assertFalse(StringUtils_.isWhiteSpace(startingString));
		startingString = "       ";
		assertTrue(StringUtils_.isWhiteSpace(startingString));
	}

	@Test
	public void testIsWhiteSpace2() {
		log.incompleteTestBecause_(this, "StringUtils_.isWhiteSpace()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testGeneratePassword() {
		log.incompleteTestBecause_(this, "StringUtils_.generatePassword()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testIsOkForPasswordRun() {
		log.incompleteTestBecause_(this, "StringUtils_.isOkForPasswordRun()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testNotIl10O() {
		log.incompleteTestBecause_(this, "StringUtils_.notIl10O()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testIsApprovedNonCharNonNumeric() {
		log.incompleteTestBecause_(this, "StringUtils_.isApprovedNonCharNonNumeric()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testIsSameCharachterRepeated4TimeMinimum() {
		String startingString = "foo________________";
		assertFalse(StringUtils_
				.isSameCharachterRepeated4TimeMinimum(startingString));
		startingString = "aaabd";
		assertFalse(StringUtils_
				.isSameCharachterRepeated4TimeMinimum(startingString));
		startingString = "aaa";
		assertFalse(StringUtils_
				.isSameCharachterRepeated4TimeMinimum(startingString));
		startingString = "aaab";
		assertFalse(StringUtils_
				.isSameCharachterRepeated4TimeMinimum(startingString));
		startingString = "aaaA";
		assertFalse(StringUtils_
				.isSameCharachterRepeated4TimeMinimum(startingString));
		startingString = "aaaa";
		assertTrue(StringUtils_
				.isSameCharachterRepeated4TimeMinimum(startingString));
		startingString = "****";
		assertTrue(StringUtils_
				.isSameCharachterRepeated4TimeMinimum(startingString));
		startingString = "====";
		assertTrue(StringUtils_
				.isSameCharachterRepeated4TimeMinimum(startingString));
		startingString = "))))";
		assertTrue(StringUtils_
				.isSameCharachterRepeated4TimeMinimum(startingString));
	
	}

	@Test
	public void testNotNullSingleCharachter() {
		log.incompleteTestBecause_(this, "StringUtils_.notNullSingleCharachter()","I swear I did all of the StringUtils tests somewhere before");
	}

	@Test
	public void testNotNullChar() {
		log.incompleteTestBecause_(this, "StringUtils_.notNullChar()","I swear I did all of the StringUtils tests somewhere before");
	}
}

// import static org.junit.Assert.*;
// import static org.mockito.Mockito.*;
// @BeforeClass public static void setUpBeforeClass() throws Exception {}
// @AfterClass public static void tearDownAfterClass() throws Exception {}
// @Before public void setUp() throws Exception {}
// @After public void tearDown() throws Exception {}
