package org.btrg.uti;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import org.btrg.uti.*;
import org.junit.Test;

public class OrderFileByAlphaLines_Test extends AbstractUtiTest {


    @Test
    public void testMain(){
        log.incompleteTestInactivePart(this, "OrderFileByAlphaLines_.main()");
    }

    @Test
    public void testOrder(){
        log.incompleteTestInactivePart(this, "OrderFileByAlphaLines_.order()");
    }

    @Test
    public void testInstance(){
        log.incompleteTestInactivePart(this, "OrderFileByAlphaLines_.instance()");
    }

    @Test
    public void testWriteFile(){
        log.incompleteTestInactivePart(this, "OrderFileByAlphaLines_.writeFile()");
    }

    @Test
    public void testFileContents(){
        log.incompleteTestInactivePart(this, "OrderFileByAlphaLines_.fileContents()");
    }
}


// import static org.junit.Assert.*;
// import static org.mockito.Mockito.*;
// @BeforeClass public static void setUpBeforeClass() throws Exception {}
// @AfterClass public static void tearDownAfterClass() throws Exception {}
// @Before public void setUp() throws Exception {}
// @After public void tearDown() throws Exception {}
