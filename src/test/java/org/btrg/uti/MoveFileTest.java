package org.btrg.uti;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.File;
import java.io.IOException;

import org.btrg.uti.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class MoveFileTest extends AbstractUtiTest {
	File seedFile = new File("./src/test/resources/docInput.txt");
	String randomName1 = null;
	File file1 = null;
	File file2 = null;
	String randomName2 = null;
	File dir = null;

	@Before
	public void setUp() throws Exception {
		randomName1 = StringUtils_.randomString(5, false);
		file1 = new File("./src/test/resources/" + randomName1 + ".txt");
		randomName2 = StringUtils_.randomString(5, false);
		dir = new File("./src/test/resources/" + randomName2);
		file2 = new File(dir.getAbsolutePath()+"/" + randomName1 + ".txt");
		System.out.println("FILE IS "+file2.getAbsolutePath());
	}

	@Test
	public void testCopyFile() {
		assertTrue(!file1.exists());
		try {
			MoveFile.copyFile(seedFile, file1);
		} catch (IOException e) {
			e.printStackTrace();
			fail();
		}
		assertTrue(file1.exists());
	}

	@Test
	public void testDelete() throws Exception{
		dir.mkdir();
		MoveFile.copyFile(seedFile, file1);
		MoveFile.copyFile(file1, file2);
		MoveFile.delete(file1);
		MoveFile.delete(dir);
		assertTrue(!file1.exists());
		assertTrue(!file2.exists());
		assertTrue(!dir.exists());
		
	}

	@Test
	public void testMoveFile() {
		assertTrue(!file1.exists());
		try {
			MoveFile.copyFile(seedFile, file1);
		} catch (IOException e) {
			e.printStackTrace();
			fail();
		}
		assertTrue(file1.exists());
		assertTrue(!dir.exists());
		MoveFile.moveFile(file1, dir.getAbsolutePath());
		assertTrue(!file1.exists());
		assertTrue(dir.exists());
	}

	@After
	public void tearDown() throws Exception {
		if (file1.exists()) {
			file1.delete();
		}
		if (file2.exists()) {
			MoveFile.delete(dir);
		}
		if (file2.exists()) {
			fail();
		}
		if (dir.exists()) {
			fail();
		}
	}

	@Test
	public void testCopyDirectory() {
		log.incompleteTestInactivePart(this, "MoveFile.copyDirectory()");
	}

	@Test
	public void testMain() {
		log.incompleteTestInactivePart(this, "MoveFile.main()");
	}
}

// import static org.junit.Assert.*;
// import static org.mockito.Mockito.*;
// @BeforeClass public static void setUpBeforeClass() throws Exception {}
// @AfterClass public static void tearDownAfterClass() throws Exception {}
// @Before public void setUp() throws Exception {}
// @After public void tearDown() throws Exception {}
