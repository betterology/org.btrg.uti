package org.btrg.uti;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import org.btrg.uti.*;
import org.junit.Test;

public class SeedFile_Test extends AbstractUtiTest {


    @Test
    public void testMain(){
        log.incompleteTestInactivePart(this, "SeedFile_.main()");
    }

    @Test
    public void testInstance(){
        log.incompleteTestInactivePart(this, "SeedFile_.instance()");
    }

    @Test
    public void testSeed(){
        log.incompleteTestInactivePart(this, "SeedFile_.seed()");
    }

    @Test
    public void testWriteFile(){
        log.incompleteTestInactivePart(this, "SeedFile_.writeFile()");
    }
}


// import static org.junit.Assert.*;
// import static org.mockito.Mockito.*;
// @BeforeClass public static void setUpBeforeClass() throws Exception {}
// @AfterClass public static void tearDownAfterClass() throws Exception {}
// @Before public void setUp() throws Exception {}
// @After public void tearDown() throws Exception {}
