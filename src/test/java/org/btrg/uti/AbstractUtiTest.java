package org.btrg.uti;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/** @author petecarapetyan */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/META-INF/spring/uti-bundle-context.xml",
		"/META-INF/spring/uti-bundle-context.xml" }, inheritLocations = true)
public abstract class AbstractUtiTest {
	@Autowired
	protected ITestLog_ log;
}