package org.btrg.uti;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import org.btrg.uti.*;
import org.junit.Test;

public class BitwiseFlag_Test extends AbstractUtiTest {


    @Test
    public void testHasFlag_(){
        log.incompleteTestLowPriority(this, "BitwiseFlag_.hasFlag_()");
    }

    @Test
    public void testToggleFlagToOnRegardlessOfCurrentState_(){
        log.incompleteTestLowPriority(this, "BitwiseFlag_.toggleFlagToOnRegardlessOfCurrentState_()");
    }

    @Test
    public void testToggleFlagToOffRegardlessOfCurrentState_(){
        log.incompleteTestLowPriority(this, "BitwiseFlag_.toggleFlagToOffRegardlessOfCurrentState_()");
    }

    @Test
    public void testToggleFlag_(){
        log.incompleteTestLowPriority(this, "BitwiseFlag_.toggleFlag_()");
    }

    @Test
    public void testPrintIntegerAsBinaryShowingAllZeroes_(){
        log.incompleteTestLowPriority(this, "BitwiseFlag_.printIntegerAsBinaryShowingAllZeroes_()");
    }

    @Test
    public void testIntegerAsBinaryShowingAllZeroesToString_(){
        log.incompleteTestLowPriority(this, "BitwiseFlag_.integerAsBinaryShowingAllZeroesToString_()");
    }
}


// import static org.junit.Assert.*;
// import static org.mockito.Mockito.*;
// @BeforeClass public static void setUpBeforeClass() throws Exception {}
// @AfterClass public static void tearDownAfterClass() throws Exception {}
// @Before public void setUp() throws Exception {}
// @After public void tearDown() throws Exception {}
