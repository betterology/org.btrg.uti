package org.btrg.uti;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import org.btrg.uti.*;
import org.junit.Test;

public class StackTraceConvenience_Test extends AbstractUtiTest {


    @Test
    public void testAdd(){
        log.incompleteTestInactivePart(this, "StackTraceConvenience_.add()");
    }

    @Test
    public void testInitialize(){
        log.incompleteTestInactivePart(this, "StackTraceConvenience_.initialize()");
    }

    @Test
    public void testGetTraceListing(){
        log.incompleteTestInactivePart(this, "StackTraceConvenience_.getTraceListing()");
    }

    @Test
    public void testGetTraceListingAsString(){
        log.incompleteTestInactivePart(this, "StackTraceConvenience_.getTraceListingAsString()");
    }

    @Test
    public void testAddStackTrace(){
        log.incompleteTestInactivePart(this, "StackTraceConvenience_.addStackTrace()");
    }
}


// import static org.junit.Assert.*;
// import static org.mockito.Mockito.*;
// @BeforeClass public static void setUpBeforeClass() throws Exception {}
// @AfterClass public static void tearDownAfterClass() throws Exception {}
// @Before public void setUp() throws Exception {}
// @After public void tearDown() throws Exception {}
