package org.btrg.uti;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import org.btrg.uti.*;
import org.junit.Test;

public class UnicodeEncoder_Test extends AbstractUtiTest {


    @Test
    public void testEncode(){
        log.incompleteTestInactivePart(this, "UnicodeEncoder_.encode()");
    }

    @Test
    public void testEncode2(){
        log.incompleteTestInactivePart(this, "UnicodeEncoder_.encode()");
    }

    @Test
    public void testIsHighSurrogate(){
        log.incompleteTestInactivePart(this, "UnicodeEncoder_.isHighSurrogate()");
    }

    @Test
    public void testIsLowSurrogate(){
        log.incompleteTestInactivePart(this, "UnicodeEncoder_.isLowSurrogate()");
    }

    @Test
    public void testToCodePoint(){
        log.incompleteTestInactivePart(this, "UnicodeEncoder_.toCodePoint()");
    }
}


// import static org.junit.Assert.*;
// import static org.mockito.Mockito.*;
// @BeforeClass public static void setUpBeforeClass() throws Exception {}
// @AfterClass public static void tearDownAfterClass() throws Exception {}
// @Before public void setUp() throws Exception {}
// @After public void tearDown() throws Exception {}
