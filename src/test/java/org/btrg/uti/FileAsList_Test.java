package org.btrg.uti;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.File;

import org.btrg.uti.*;
import org.junit.Test;

public class FileAsList_Test extends AbstractUtiTest {

	@Test
	public void testRead() {
		File file = new File("./src/test/resources/docInput.txt");
		assertTrue(file.exists());
		assertEquals(2644, FileAsList_.read(file).size());
	}

	@Test
	public void testRead2() {
		assertEquals(2644, FileAsList_
				.read("./src/test/resources/docInput.txt").size());
	}

	@Test
	public void testReadShift6back() {
		File file = new File("./src/test/resources/docInput.txt");
		assertTrue(file.exists());
		assertEquals(2575, FileAsList_.readShift6back(file).size());
		assertTrue(FileAsList_.readShift6back(file).get(22).contains("msh]blihct_^z\\iif_[hz?=lo^J[lnY(cmP[fc^Y"));
	}
}

// import static org.junit.Assert.*;
// import static org.mockito.Mockito.*;
// @BeforeClass public static void setUpBeforeClass() throws Exception {}
// @AfterClass public static void tearDownAfterClass() throws Exception {}
// @Before public void setUp() throws Exception {}
// @After public void tearDown() throws Exception {}
