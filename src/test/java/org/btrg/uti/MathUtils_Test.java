package org.btrg.uti;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import org.btrg.uti.*;
import org.junit.Test;

public class MathUtils_Test extends AbstractUtiTest {


    @Test
    public void testDecimalMultiplication(){
        log.incompleteTestInactivePart(this, "MathUtils_.decimalMultiplication()");
    }

    @Test
    public void testDecimalDivision(){
        log.incompleteTestInactivePart(this, "MathUtils_.decimalDivision()");
    }
}


// import static org.junit.Assert.*;
// import static org.mockito.Mockito.*;
// @BeforeClass public static void setUpBeforeClass() throws Exception {}
// @AfterClass public static void tearDownAfterClass() throws Exception {}
// @Before public void setUp() throws Exception {}
// @After public void tearDown() throws Exception {}
