package org.btrg.uti;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import org.btrg.uti.*;
import org.junit.Test;

public class TestLogTest extends AbstractUtiTest {


    @Test
    public void testLog(){
        log.incompleteTestLowPriority(this, "TestLog.log()");
    }

    @Test
    public void testGetPackageName(){
        log.incompleteTestLowPriority(this, "TestLog.getPackageName()");
    }

    @Test
    public void testincompleteTestLowPriority(){
        log.incompleteTestLowPriority(this, "TestLog.incompleteTestLowPriority()");
    }

    @Test
    public void testIncompleteTestLowPriority(){
        log.incompleteTestLowPriority(this, "TestLog.incompleteTestLowPriority()");
    }

    @Test
    public void testIncompleteTestInactiveProject(){
        log.incompleteTestLowPriority(this, "TestLog.incompleteTestInactiveProject()");
    }

    @Test
    public void testIncompleteTestBecause_(){
        log.incompleteTestLowPriority(this, "TestLog.incompleteTestBecause_()");
    }

    @Test
    public void testIncompleteCode_(){
        log.incompleteTestLowPriority(this, "TestLog.incompleteCode_()");
    }

    @Test
    public void testTestNotRequired_(){
        log.incompleteTestLowPriority(this, "TestLog.testNotRequired_()");
    }

    @Test
    public void testToConsole_(){
        log.incompleteTestLowPriority(this, "TestLog.toConsole_()");
    }

    @Test
    public void testIncompleteUiTest(){
        log.incompleteTestLowPriority(this, "TestLog.incompleteUiTest()");
    }
}


// import static org.junit.Assert.*;
// import static org.mockito.Mockito.*;
// @BeforeClass public static void setUpBeforeClass() throws Exception {}
// @AfterClass public static void tearDownAfterClass() throws Exception {}
// @Before public void setUp() throws Exception {}
// @After public void tearDown() throws Exception {}
