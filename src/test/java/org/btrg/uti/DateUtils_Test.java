package org.btrg.uti;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import org.btrg.uti.*;
import org.junit.Test;

public class DateUtils_Test extends AbstractUtiTest {


    @Test
    public void testGetMonth(){
        log.incompleteTestLowPriority(this, "DateUtils_.getMonth()");
    }

    @Test
    public void testGetYear(){
        log.incompleteTestLowPriority(this, "DateUtils_.getYear()");
    }

    @Test
    public void testGetDayOfMonth(){
        log.incompleteTestLowPriority(this, "DateUtils_.getDayOfMonth()");
    }

    @Test
    public void testGetDayOfWeek(){
        log.incompleteTestLowPriority(this, "DateUtils_.getDayOfWeek()");
    }

    @Test
    public void testGetDateFromYyyyMMdd(){
        log.incompleteTestLowPriority(this, "DateUtils_.getDateFromYyyyMMdd()");
    }

    @Test
    public void testPrefixedWithYyMMdd(){
        log.incompleteTestLowPriority(this, "DateUtils_.prefixedWithYyMMdd()");
    }

    @Test
    public void testIsDateWithin30Minutes(){
        log.incompleteTestLowPriority(this, "DateUtils_.isDateWithin30Minutes()");
    }

    @Test
    public void testToUtilDate(){
        log.incompleteTestLowPriority(this, "DateUtils_.toUtilDate()");
    }

    @Test
    public void testGetDateFromYyMMdd(){
        log.incompleteTestLowPriority(this, "DateUtils_.getDateFromYyMMdd()");
    }

    @Test
    public void testGetDateFromMmDashDdDashYy(){
        log.incompleteTestLowPriority(this, "DateUtils_.getDateFromMmDashDdDashYy()");
    }

    @Test
    public void testGetDateFromYyyyMMddHHmmss(){
        log.incompleteTestLowPriority(this, "DateUtils_.getDateFromYyyyMMddHHmmss()");
    }

    @Test
    public void testGetDateFromYyyyMMddHHmmssZeroHours(){
        log.incompleteTestLowPriority(this, "DateUtils_.getDateFromYyyyMMddHHmmssZeroHours()");
    }

    @Test
    public void testGetDateFromdd_MMM_yy(){
        log.incompleteTestLowPriority(this, "DateUtils_.getDateFromdd_MMM_yy()");
    }

    @Test
    public void testToSqlDate(){
        log.incompleteTestLowPriority(this, "DateUtils_.toSqlDate()");
    }

    @Test
    public void testToSqlDate2(){
        log.incompleteTestLowPriority(this, "DateUtils_.toSqlDate()");
    }

    @Test
    public void testRetreatYear(){
        log.incompleteTestLowPriority(this, "DateUtils_.retreatYear()");
    }

    @Test
    public void testGetXXYear(){
        log.incompleteTestLowPriority(this, "DateUtils_.getXXYear()");
    }

    @Test
    public void testGetNumberDaysFromToday(){
        log.incompleteTestLowPriority(this, "DateUtils_.getNumberDaysFromToday()");
    }

    @Test
    public void testDateSecondsDifference(){
        log.incompleteTestLowPriority(this, "DateUtils_.dateSecondsDifference()");
    }

    @Test
    public void testGetCurrentTimeStamp(){
        log.incompleteTestLowPriority(this, "DateUtils_.getCurrentTimeStamp()");
    }

    @Test
    public void testFollowingDay(){
        log.incompleteTestLowPriority(this, "DateUtils_.followingDay()");
    }

    @Test
    public void testNHoursLater(){
        log.incompleteTestLowPriority(this, "DateUtils_.nHoursLater()");
    }

    @Test
    public void testGetMidnight(){
        log.incompleteTestLowPriority(this, "DateUtils_.getMidnight()");
    }

    @Test
    public void testGetMidnight2(){
        log.incompleteTestLowPriority(this, "DateUtils_.getMidnight()");
    }
}


// import static org.junit.Assert.*;
// import static org.mockito.Mockito.*;
// @BeforeClass public static void setUpBeforeClass() throws Exception {}
// @AfterClass public static void tearDownAfterClass() throws Exception {}
// @Before public void setUp() throws Exception {}
// @After public void tearDown() throws Exception {}
