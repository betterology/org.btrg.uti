package org.btrg.uti;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import org.btrg.uti.*;
import org.junit.Test;

public class OutputStreamConverter_Test extends AbstractUtiTest {


    @Test
    public void testAccess$202(){
        log.incompleteTestInactivePart(this, "OutputStreamConverter_.access$202()");
    }

    @Test
    public void testAccess$000(){
        log.incompleteTestInactivePart(this, "OutputStreamConverter_.access$000()");
    }

    @Test
    public void testAccess$102(){
        log.incompleteTestInactivePart(this, "OutputStreamConverter_.access$102()");
    }

    @Test
    public void testInitialize(){
        log.incompleteTestInactivePart(this, "OutputStreamConverter_.initialize()");
    }

    @Test
    public void testGetOutputStream(){
        log.incompleteTestInactivePart(this, "OutputStreamConverter_.getOutputStream()");
    }

    @Test
    public void testDelay(){
        log.incompleteTestInactivePart(this, "OutputStreamConverter_.delay()");
    }
}


// import static org.junit.Assert.*;
// import static org.mockito.Mockito.*;
// @BeforeClass public static void setUpBeforeClass() throws Exception {}
// @AfterClass public static void tearDownAfterClass() throws Exception {}
// @Before public void setUp() throws Exception {}
// @After public void tearDown() throws Exception {}
