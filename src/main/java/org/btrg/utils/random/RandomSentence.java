package org.btrg.utils.random;

import java.io.IOException;
import java.io.RandomAccessFile;

import org.btrg.uti.RandomIntGenerator_;
import org.btrg.uti.StringUtils_;

public class RandomSentence {
	
	private RandomSentence(){
	}
	
	private static RandomSentence instance;

	public static String getSentence() {
		if(instance==null){
			instance = new RandomSentence();
		}
		int sentenceLength = RandomIntGenerator_.get(10);
		StringBuffer output = new StringBuffer();
		output.append(getFirstWord()+ " ");
		for(int i = 0;i<sentenceLength;i++){
			output.append(getCleanWord()+ " ");
		}
		String sentence = output.toString().trim();
        if(sentence.length()>255){
            sentence = sentence.substring(0,255);
        }
		sentence = sentence + ". ";
		return sentence;
	}


	private static Object getFirstWord() {
		String word = getCleanWord();
		return StringUtils_.upLow(word);
	}

	private static String getCleanWord() {
		String word = "fantastic";
		for(int i = 0;i<10;i++){
			word = RandomWord.getWord();
			if(word.endsWith("-")||word.endsWith(".")){
				continue;
			}else{
				break;
			}
		}
		return word;
	}
}
