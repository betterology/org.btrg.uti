package org.btrg.uti;

import java.math.BigDecimal;
import java.math.MathContext;

/* Simple utilities class for benchmarking unit tests. 
 * Use is a follows: 
 * TestTimer t = new TestTimer("My task");  // Do something
 * t.done();
 */
public class TestTimer_ {
	private long startTime;
	private String message;

	/**
	 * * Initiate a timer
	 */
	public TestTimer_(String message) {
		startTime = System.currentTimeMillis();
		this.message = message;
	}

	/**
	 * * Reset the timer for another timing session. 
	 */
	public void reset() {
		startTime = System.currentTimeMillis();
	}

	public void reset(String message) {
		startTime = System.currentTimeMillis();
		this.message = message;
	}

	/**
	 * * End the timing session and output the results. 
	 */
	public void doneMinutes() {
		System.out.println(doneMinutesMessage());
	}

	/**
	 * * End the timing session and output the results. 
	 */
	public void doneSeconds() {
		System.out.println(doneSecondsMessage());
	}

	public long difference() {
		return System.currentTimeMillis() - startTime;
	}

	public BigDecimal elapsedMinutes() {
		double ms = (double) System.currentTimeMillis() - startTime;
		ms = ms / 60000;
		BigDecimal elapsedMinutes = new BigDecimal(ms).setScale(3,
				BigDecimal.ROUND_HALF_UP);
		// elapsedMinutes = elapsedMinutes.divide(new BigDecimal(63),
		// elapsedMinutes.ROUND_HALF_UP).setScale(2,
		// BigDecimal.ROUND_HALF_UP);
		return elapsedMinutes;
	}

	public String elapsedSeconds() {
		String time = String.format("%07d", difference());
		time = time.substring(0, 7 - 3) + "." + time.substring(7 - 3, 7);
		while (time.startsWith("0")) {
			time = time.substring(1, time.length());
		}
		if (time.startsWith(".")) {
			time = "0" + time;
		}
		return time;
	}

	public String doneSecondsMessage() {
		return message + " : " + elapsedSeconds() + " Seconds";
	}

	public String doneMinutesMessage() {
		return message + " : " + elapsedMinutes() + " Minutes";
	}
}
