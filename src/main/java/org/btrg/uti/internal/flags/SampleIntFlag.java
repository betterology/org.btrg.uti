package org.btrg.uti.internal.flags;


/**
 * This is a set of flags that can always be used with each other 00001000100
 * etc yada. They are arbitrarily set with fake names that are not intended for
 * real use. When you use one, rename it to MY_FOO_BAR_WAH or similar constant
 * name. It is assumed that we won't ever get to use all 31 types.
 */
public interface SampleIntFlag {
	public static final int INT_1 = 1;
	public static final int INT_2 = 2;
	public static final int INT_4 = 4;
	public static final int INT_8 = 8;
	public static final int INT_16 = 16;
	public static final int INT_32 = 32;
	public static final int INT_64 = 64;
	public static final int INT_128 = 128;
	public static final int INT_256 = 256;
	public static final int INT_512 = 512;
	public static final int INT_1024 = 1024;
	public static final int INT_2048 = 2048;
	public static final int INT_4096 = 4096;
	public static final int INT_8192 = 8192;
	public static final int INT_16384 = 16384;
	public static final int INT_32768 = 32768;
	public static final int INT_65536 = 65536;
	public static final int INT_131072 = 131072;
	public static final int INT_262144 = 262144;
	public static final int INT_524288 = 524288;
	public static final int INT_1048576 = 1048576;
	public static final int INT_2097152 = 2097152;
	public static final int INT_4194304 = 4194304;
	public static final int INT_8388608 = 8388608;
	public static final int INT_16777216 = 16777216;
	public static final int INT_33554432 = 33554432;
	public static final int INT_67108864 = 67108864;
	public static final int INT_134217728 = 134217728;
	public static final int INT_268435456 = 268435456;
	public static final int INT_536870912 = 536870912;
	public static final int INT_1073741824 = 1073741824;
}
