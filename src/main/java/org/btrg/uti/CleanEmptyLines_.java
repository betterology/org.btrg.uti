package org.btrg.uti;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;


/**
 * <h2>CleanEmptyLines functional summary</h2>
 * This is a hack convenience class for cleaning a file
 * of empty lines.
 *
 * <br>
 * To use this class
 * <br>
 * <b>CleanEmptyLines.instance().clean(mySourcePath);</b>
 *
 * @author Pete Carapetyan pete@datafundamentals.com
 */
public class CleanEmptyLines_ {
   private static CleanEmptyLines_ uniqueInstance= null;

   private CleanEmptyLines_() {
      super();
   }

   public static CleanEmptyLines_ instance() {
      if (uniqueInstance == null) {
         uniqueInstance= new CleanEmptyLines_();
      }
      return uniqueInstance;
   }

   public synchronized void clean(String fullFilePath) {
      File existingFile= new File(fullFilePath);
      try {
         if (null == fullFilePath || fullFilePath.length() < 1) {
            throw new RuntimeException("An attempt was made to initialize a file without passing it a file path");
         }
         FileInputStream fileContentStream=
            new FileInputStream(new File(fullFilePath));
         BufferedInputStream bufferedInputStream=
            new BufferedInputStream(fileContentStream);
         String fileContents= fileContents(bufferedInputStream);
         fileContents= StringUtils_.removeEmptyLines(fileContents);
         bufferedInputStream= null;
         fileContentStream= null;
         existingFile.delete();
         writeFile(fullFilePath, fileContents);
      } catch (FileNotFoundException e) {
         throw new RuntimeException(
            "CleanEmptyLines.clean() was instructed to open a file, but it could not. Please check this path: '"
               + fullFilePath
               + "'");
      } catch (IOException e) {
         e.printStackTrace();
         e.getCause().printStackTrace();
         throw new RuntimeException(
            "IOException on filePath '" + fullFilePath + "'\n\n" + e);
      }
   }

   private void writeFile(String fullFilePath, String fileContents)
      throws IOException {
      File file= new File(fullFilePath);
      File parentDir= file.getParentFile();
      if (parentDir != null) {
         parentDir.mkdirs();
      }
      FileOutputStream outputFile= new FileOutputStream(fullFilePath);
      OutputStreamWriter writer= new OutputStreamWriter(outputFile);
      BufferedWriter bufferedWriter= new BufferedWriter(writer);
      PrintWriter printWriter= new PrintWriter(bufferedWriter, true);
      printWriter.print(fileContents);
      printWriter.close();
   }

   private String fileContents(InputStream fileContentStream) {
      byte[] byteContentArray= null;
      try {
         byteContentArray= new byte[fileContentStream.available()];
         fileContentStream.read(
            byteContentArray,
            0,
            fileContentStream.available());
      } catch (IOException e) {
         e.printStackTrace();
         e.getCause().printStackTrace();
         throw new RuntimeException("CleanEmptyLines.fileContents() was passed unable to open the InputStream to read the byte array.");
      }
      return new String(byteContentArray);
   }

   public static void main(String[] args) {
      String fullSourcePath= "C:/keel/app-poll/conf/model-roles.xconf";
      new CleanEmptyLines_().clean(fullSourcePath);
   }

}
