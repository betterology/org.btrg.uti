package org.btrg.uti;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class FileAsString_ {
	
   public static synchronized String read(File file) throws FileNotFoundException, IOException {
      BufferedReader bufferedReader;
      StringBuffer sb= new StringBuffer(10000);
      String getLine= null;
      bufferedReader= new BufferedReader(new FileReader(file));
      getLine= bufferedReader.readLine();
      sb.append(getLine);
      while (null != getLine) {
         getLine= bufferedReader.readLine();
         if (null != getLine) {
            sb.append("\n" + getLine);
         }
      }
      return sb.toString();
   }

   public static synchronized String read(String filePath) throws FileNotFoundException, IOException {
      return read(new File((filePath)));
   }

}
