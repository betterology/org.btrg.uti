package org.btrg.uti;

import java.util.TreeSet;

public class GeneratePW_ {

	public static void main(String[] args) {
		GeneratePW_.go();
	}

	private static void go() {
		TreeSet<String> index = new TreeSet<String>();
		TreeSet<String> userName = new TreeSet<String>();
		TreeSet<String> password = new TreeSet<String>();
		Object[] indexArray;
		Object[] userNameArray;
		Object[] passwordArray;

		try {
			StringBuffer sBuffer = new StringBuffer();
			for (int i = 0; i < 255; i++) {
				char oneChar = (char) i;
			}
			for (int i = 0; i < 500; i++) {
				index.add(getIndex(i));
				userName.add(StringUtils_.username(7));
				password.add(StringUtils_.generatePassword(8, true));
			}
			indexArray = index.toArray();
			userNameArray = userName.toArray();
			passwordArray = password.toArray();
			int max = 125;
			int i = 0;
			String line = null;
			for (int j = 0; j < max; j++) {
				line = "";
				for (int k = 0; k < 4; k++) {
					i = j + (max * k);
					line = line + indexArray[i] + " " + userNameArray[i] + " "
							+ passwordArray[i] + "  ";
				}
				sBuffer.append(line.trim() + "\n");
			}
			new FileWrite_("pw.txt", sBuffer.toString());
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}

	private static String getIndex(int i) {
		String index = "" + i;
		while (index.length() < 3) {
			index = "0" + index;
		}
		return index;
	}
}
