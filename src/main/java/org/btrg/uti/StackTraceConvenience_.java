package org.btrg.uti;

import java.util.ArrayList;
import java.util.List;

public class StackTraceConvenience_ {
	private List<String> traceListing;

	StackTraceConvenience_(Throwable throwable, boolean alsoAddCause) {
		initialize();
		addStackTrace(throwable, alsoAddCause);
	}

	StackTraceConvenience_() {
		initialize();
	}

	public void add(Throwable throwable, boolean alsoAddCause) {
		addStackTrace(throwable, alsoAddCause);
	}

	public List getTraceListing() {
		return traceListing;
	}

	public String getTraceListingAsString() {
		StringBuffer stringBuffer = new StringBuffer(2000);
		for (String line : traceListing) {
			stringBuffer.append(line + "\n");
		}
		return stringBuffer.toString();
	}

	private void initialize() {
		if (null == traceListing) {
			traceListing = new ArrayList<String>();
		}
	}

	private void addStackTrace(Throwable throwable, boolean alsoAddCause) {
		StackTraceElement[] stackTraceElements = throwable.getStackTrace();
		traceListing.add("- ERROR -");
		for (int i = 0; i < stackTraceElements.length; i++) {
			traceListing.add(stackTraceElements[i].toString());
		}
		if (alsoAddCause && null != throwable.getCause()
				&& null != throwable.getCause().getStackTrace()) {
			stackTraceElements = throwable.getCause().getStackTrace();
			traceListing.add("- ERROR.CAUSE -");
			for (int i = 0; i < stackTraceElements.length; i++) {
				traceListing.add(stackTraceElements[i].toString());
			}
		}
	}

}
