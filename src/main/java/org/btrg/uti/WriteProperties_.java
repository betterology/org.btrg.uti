package org.btrg.uti;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * <h2>WriteXml - a convenience class</h2>
 * 
 * This is a convenience class only, which provides a single call
 * to write properties without instantiating File  
 * 
 * To use <br>
 * WriteProperites.instance().write(fullFilePath, myProperties)
 * 
 * @author Pete Carapetyan pete@datafundamentals.com
 */
public class WriteProperties_ {
   private static WriteProperties_ uniqueInstance= null;
   private WriteProperties_() {
      super();
   }
   public static WriteProperties_ instance() {
      if (uniqueInstance == null) {
         uniqueInstance= new WriteProperties_();
      }
      return uniqueInstance;
   }

   public synchronized void write(
      String fullFilePath,
      Properties myProperties,
      String headerMessage) {
      File file= new File(fullFilePath);
      File parentDir= file.getParentFile();
      if (parentDir != null) {
         parentDir.mkdirs();
      }
      FileOutputStream fileOutputStream= null;
      try {
         fileOutputStream= new FileOutputStream(file);
      } catch (FileNotFoundException fileNotFoundException) {
         fileNotFoundException.printStackTrace();
         fileNotFoundException.getCause().printStackTrace();
         throw new RuntimeException(
            "WriteXml threw a FileNotFoundException for the file '"
               + fullFilePath
               + "'");
      }
      try {
         myProperties.store(fileOutputStream, headerMessage);
      } catch (IOException e) {
         e.printStackTrace();
         throw new RuntimeException(
            "WriteProperties.write() threw an exception when trying to write a file with the path '"
               + fullFilePath
               + "'");
      }
   }

   public static void main(String[] args) {
      Properties properties= new Properties();
      properties.setProperty("lfds", "he");
      WriteProperties_.instance().write(
         "/suite/deleteme/blahfull.properties",
         properties,
         "hi");
   }
}
