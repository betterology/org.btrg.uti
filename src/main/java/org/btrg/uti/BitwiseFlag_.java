package org.btrg.uti;

public class BitwiseFlag_ {

	public static boolean hasFlag_(int flags_, int flagToCheck_) {
		return ((flags_ & flagToCheck_) == flagToCheck_);
	}

	public static int toggleFlag_(int flags_, int flagToRemove_) {
		return flags_ ^ flagToRemove_;
	}

	public static void printIntegerAsBinaryShowingAllZeroes_(int integerToPrint_) {
		System.err
				.println(integerAsBinaryShowingAllZeroesToString_(integerToPrint_));
	}

	public static String integerAsBinaryShowingAllZeroesToString_(
			int integerToPrint_) {
		String valueWithoutLeadingZeroes = Integer
				.toBinaryString(integerToPrint_);
		StringBuffer sb = new StringBuffer(35);
		for (int i = 31 - valueWithoutLeadingZeroes.length(); i > -1; i--) {
			sb.append("0");
		}
		sb.append(valueWithoutLeadingZeroes);
		return sb.toString();
	}

	public static int toggleFlagToOffRegardlessOfCurrentState_(int flags_,
			int flagToSet_) {
		int returnValue = flags_;
		if (hasFlag_(flags_, flagToSet_)) {
			returnValue = toggleFlag_(flags_, flagToSet_);
		}
		return returnValue;
	}

	public static int toggleFlagToOnRegardlessOfCurrentState_(int flags_,
			int flagToSet_) {
		int returnValue = flags_;
		if (!hasFlag_(flags_, flagToSet_)) {
			returnValue = toggleFlag_(flags_, flagToSet_);
		}
		return returnValue;
	}
}
