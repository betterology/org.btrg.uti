package org.btrg.uti;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * A convenience class with one method only.
 * For passing a valid path and getting a Properties object in return
 * 
 * @author Pete Carapetyan
 */

public class LoadProperties_ {
   private Properties properties= new Properties();
   private static LoadProperties_ uniqueInstance = null;
   private LoadProperties_() {
      super();
   }

   public static LoadProperties_ instance() {
      if (uniqueInstance == null) {
         uniqueInstance = new LoadProperties_();
      }
      return uniqueInstance;
   }

   public synchronized Properties getProperties(String filePath) {
//      System.print.println("\n...Loading all properties from file '" + filePath + "'");
      try {
         if (null == filePath || filePath.length() < 1) {
            throw new RuntimeException("An attempt was made to initialize a properties file without passing it a file path");
         }
         FileInputStream fileContentStream=
            new FileInputStream(new File(filePath));
         properties.load(fileContentStream);
         //new DbSchemaGen().validateProperties();         
      } catch (FileNotFoundException e) {
         throw new RuntimeException(
            "LoadProperties was instructed to open a file, but it could not. Please check this path: '"
               + filePath
               + "'");
      } catch (IOException e) {
         e.printStackTrace();
         e.getCause().printStackTrace();
         throw new RuntimeException("IOException on filePath '" + filePath + "'\n\n" + e);
      }
      return properties;
   }

}
