package org.btrg.uti;

public class TestLog implements ITestLog_ {
	private final String INCOMPLETE_TEST = "\tINCOMPLETE TEST: ";
	private final String INCOMPLETE_TEST_LOW_PRIORITY = "\tINCOMPLETE TEST LOW PRIORITY: ";
	private final String INCOMPLETE_TEST_INACTIVE_PROJECT = "\tINCOMPLETE TEST INACTIVE PROJECT: ";
	private final String INCOMPLETE_TEST_BECAUSE = "\tINCOMPLETE TEST BECAUSE: ";
	private final String INCOMPLETE_UI_TEST = "\tINCOMPLETE TEST UI: ";
	private final String INCOMPLETE_CODE = "\tINCOMPLETE CODE: ";
	private final String INCOMPLETE_CODE_BECAUSE = "\tINCOMPLETE CODE BECAUSE: ";
	private final String TEST_NOT_REQUIRED = "\tTEST NOT REQUIRED: ";
	private final String TEST_NOT_WRITTEN = "\t(TEST NOT WRITTEN YET) ";

	private void log(String prefix, Class clasz, String methodName_,
			String statement_) {
		System.out.println(prefix + getPackageName(clasz) + "."
				+ clasz.getSimpleName() + "\n\t\t" + methodName_ + "\t"
				+ statement_);
	}

	private String getPackageName(Class clasz) {
		String packageName = "" + clasz.getPackage();
		return packageName.substring(8, packageName.length());
	}

	
	public void incompleteTest_(Object this_, String thisMethodName_) {
		log(INCOMPLETE_TEST, this_.getClass(), thisMethodName_,
				TEST_NOT_WRITTEN);
	}

	
	public void incompleteTestBecause_(Object this_, String thisMethodName_,
			String statement_) {
		log(INCOMPLETE_TEST_BECAUSE, this_.getClass(), thisMethodName_,
				statement_);
	}

	
	public void incompleteUiTest(Object this_, String thisMethodName_) {
		log(INCOMPLETE_UI_TEST, this_.getClass(), thisMethodName_,
				TEST_NOT_WRITTEN);
	}

	
	public void incompleteCodeBecause_(Object this_, String thisMethodName_,
			String statement_) {
		log(INCOMPLETE_CODE_BECAUSE, this_.getClass(), thisMethodName_,
				statement_);
	}

	
	public void toConsole_(String message_) {
		System.out.println("~ " + message_);
	}

	
	public void testNotRequired_(Object this_, String thisMethodName_,
			String statement_) {
		log(TEST_NOT_REQUIRED, this_.getClass(), thisMethodName_, statement_);

	}

	
	public void incompleteTestLowPriority(Object this_, String thisMethodName_) {
		log(INCOMPLETE_TEST_LOW_PRIORITY, this_.getClass(), thisMethodName_,
				TEST_NOT_WRITTEN);

	}

	
	public void incompleteTestInactivePart(Object this_, String thisMethodName_) {
		log(INCOMPLETE_TEST_INACTIVE_PROJECT, this_.getClass(),
				thisMethodName_, TEST_NOT_WRITTEN);

	}

	
	public void incompleteCode_(Object this_, String thisMethodName_) {
		log(INCOMPLETE_CODE, this_.getClass(), thisMethodName_,
				"");

	}

}
