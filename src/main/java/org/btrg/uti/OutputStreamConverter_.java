package org.btrg.uti;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedOutputStream;
import java.util.logging.Logger;

public class OutputStreamConverter_ {
	private static final Logger log = Logger.getLogger("OutputStreamConverter");
	/*
	 * see
	 * http://www.java-forums.org/java-tips/5327-converting-inputstream-outputstream
	 * .html
	 * 
	 * I did not check this for logic or test it out
	 */
	private OutputStream outputStream;
	private InputStream inputStream;
	private boolean complete = false;
	int writeAttempts = 0;

	public OutputStreamConverter_(InputStream inputStream) {
		this.inputStream = inputStream;
		initialize();
	}

	private void initialize() {
		new Thread(new Runnable() {
			PipedOutputStream out = new PipedOutputStream();

			public void run() {
				int nextChar;
				try {
					while ((nextChar = inputStream.read()) != -1)
						out.write(nextChar);
					out.write('\n');
					outputStream = out;
				} catch (IOException e) {
					e.printStackTrace();
					throw new RuntimeException(e);
				} finally {
					try {
						out.flush();
						out.close();
						complete = true;
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}).start();
	}

	public OutputStream getOutputStream() {
		delay();
		return outputStream;
	}

	private void delay() {
		String errorMessage = "Gave up on OutputStream conversion, as guessed that it hung due to long period of time consumed";
		while (!complete) {
			try {
				Thread.sleep(100);
				if (writeAttempts++ > 10000) {
					log.fine(errorMessage);
					throw new RuntimeException(errorMessage);
				}
			} catch (InterruptedException e) {
				errorMessage = "InterruptedException";
				log.fine(errorMessage);
				throw new RuntimeException(errorMessage);
			}
		}
	}
}
