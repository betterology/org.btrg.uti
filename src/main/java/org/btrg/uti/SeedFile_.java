package org.btrg.uti;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

/**
 * <h2>SeedFile functional summary}</h2> This is a convenience class for making
 * sure a file exists. <br>
 * <br>
 * It will create a file, if it does not exist. <br>
 * <br>
 * It has only one method, seed(String fullFilePath, String contents)
 * 
 * To use this class, use this <b>SeedFile.instance().seed(fullFilePath,
 * contents);</b>
 * 
 * @author Pete Carapetyan pete@datafundamentals.com
 */
public class SeedFile_ {
	private static SeedFile_ uniqueInstance = null;

	private SeedFile_() {
		super();
	}

	public static SeedFile_ instance() {
		if (uniqueInstance == null) {
			uniqueInstance = new SeedFile_();
		}
		return uniqueInstance;
	}

	public synchronized void seed(String fullFilePath, String contents) {
		File newFile = null;
		try {
			if (null == fullFilePath || fullFilePath.length() < 1) {
				throw new RuntimeException(
						"An attempt was made to initialize a file without passing it a file path");
			}
			newFile = new File(fullFilePath);
			if (!newFile.exists()) {
				writeFile(fullFilePath, contents);
			}
		} catch (IOException e) {
			e.printStackTrace();
			e.getCause().printStackTrace();
			throw new RuntimeException("IOException on filePath '"
					+ fullFilePath + "'\n\n" + e);
		}
	}

	private void writeFile(String filePathName, String fileContents)
			throws IOException {
		// originally written to use ant normalize see project
		// org.btrg.utils.ant
		// File destination= FileUtils.newFileUtils().normalize(filePathName);
		File destination = new File(filePathName);
		FileOutputStream outputFile = new FileOutputStream(filePathName);
		OutputStreamWriter writer = new OutputStreamWriter(outputFile);
		BufferedWriter bufferedWriter = new BufferedWriter(writer);
		PrintWriter printWriter = new PrintWriter(bufferedWriter, true);
		printWriter.print(fileContents);
		printWriter.close();
	}

	public static void main(String[] args) {
		new SeedFile_().seed("C:/suite/webappwriter/conf/test.xml", "test");
	}

}
