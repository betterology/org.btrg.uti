package org.btrg.uti;

import java.math.BigInteger;

public class Remainder_ {

	public synchronized static boolean has(int number, int denominator) {
		boolean returnValue = true;
		BigInteger foo = BigInteger.valueOf(number);
		if (BigInteger.valueOf(0L).compareTo(
				foo.remainder(BigInteger.valueOf(denominator))) == 0) {
			returnValue = false;
		}
		return returnValue;
	}

}
