package org.btrg.uti;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
public class FileWrite_ {
   
   //this class might work faster using NIO 
   
   private FileWrite_() {
   }

   public FileWrite_(String filePathName, String text){
      try {
  		// originally written to use ant normalize see project
  		// org.btrg.utils.ant
  		// File destination= FileUtils.newFileUtils().normalize(filePathName);
  		File destination = new File(filePathName);
         destination.mkdirs();
         if (!destination.delete()) {
            throw new IOException(
               ":Unable to delete existing destination file '" + filePathName);
         }
         FileOutputStream outputFile= new FileOutputStream(filePathName);
         OutputStreamWriter writer= new OutputStreamWriter(outputFile);
         BufferedWriter bufferedWriter= new BufferedWriter(writer);
         PrintWriter printWriter= new PrintWriter(bufferedWriter, true);
         printWriter.print(text);
         printWriter.close(); 
            
      } catch (IOException ioe) {
         throw new RuntimeException(
            "FileWrite exception with file named '" + filePathName + "'" + ioe);
      }
   }
}
