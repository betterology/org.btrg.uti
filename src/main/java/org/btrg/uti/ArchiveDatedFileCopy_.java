package org.btrg.uti;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;


/**
 * <h2>ArchiveDatedFileCopy functional summary</h2>
 * This is a convenience class for saving a copy of a file before
 * you work on it.
 * <br><br>
 * It will save a copy of your file named myfile.xxx to myWriteFolder/myfile.xxxYYYYMMDDHHMM
 * <br><br>
 * It has one primary method, writeArchive(String filePath, String archiveFolderPath)
 * <br><br>
 * To use this class:
 * <br>
 * <b>ArchiveDatedFileCopy.instance().writeArchive(mySourcePath, myWriteFolderPath);</b>
 *
 * @author Pete Carapetyan pete@datafundamentals.com
 */
public class ArchiveDatedFileCopy_ {
   private static ArchiveDatedFileCopy_ uniqueInstance= null;

   private ArchiveDatedFileCopy_() {
      super();
   }

   public static ArchiveDatedFileCopy_ instance() {
      if (uniqueInstance == null) {
         uniqueInstance= new ArchiveDatedFileCopy_();
      }
      return uniqueInstance;
   }

   public synchronized void writeArchive(
      String fullPath,
      String archiveFolderPath) {
      File pathExtractor= new File(fullPath);
      writeArchive(
         pathExtractor.getName(),
         pathExtractor.getParentFile().getPath(),
         archiveFolderPath);
   }

   public synchronized void writeArchive(
      String fileName,
      String fileSourceFolderPath,
      String archiveFolderPath) {
      fileSourceFolderPath= StringUtils_.endSingleSlash(fileSourceFolderPath);
      archiveFolderPath= StringUtils_.endSingleSlash(archiveFolderPath);
      String fileSourcePath= fileSourceFolderPath + fileName;
      String fileWritePath=
         archiveFolderPath + fileName + StringUtils_.yyMMddHHmmssPrint();
      try {
         if (null == fileSourcePath || fileSourcePath.length() < 1) {
            throw new RuntimeException("An attempt was made to initialize a file without passing it a file path");
         }
         FileInputStream fileContentStream=
            new FileInputStream(new File(fileSourcePath));
         BufferedInputStream bufferedInputStream=
            new BufferedInputStream(fileContentStream);
         writeFile(fileWritePath, bufferedInputStream);
      } catch (FileNotFoundException e) {
         throw new RuntimeException(
            "ArchiveDatedFileCopy.writeArchive() was instructed to open a file, but it could not. Please check this path: '"
               + fileSourcePath
               + "'");
      } catch (IOException e) {
         e.printStackTrace();
         e.getCause().printStackTrace();
         throw new RuntimeException(
            "IOException on filePath '" + fileSourcePath + "'\n\n" + e);
      }
   }

   private void writeFile(String fullFilePath, InputStream fileContentStream)
      throws IOException {
      File file= new File(fullFilePath);
      File parentDir= file.getParentFile();
      if (parentDir != null) {
         parentDir.mkdirs();
      }
      FileOutputStream outputFile= new FileOutputStream(fullFilePath);
      OutputStreamWriter writer= new OutputStreamWriter(outputFile);
      BufferedWriter bufferedWriter= new BufferedWriter(writer);
      byte[] byteContentArray= new byte[fileContentStream.available()];
      fileContentStream.read(
         byteContentArray,
         0,
         fileContentStream.available());
      PrintWriter printWriter= new PrintWriter(bufferedWriter, true);
      printWriter.print(new String(byteContentArray));
      printWriter.close();
   }

   public static void main(String[] args) {
      String fullSourcePath=
         "C:/suite/webappwriter/conf/keel-view-contract.xml";
      new ArchiveDatedFileCopy_().writeArchive(
         "keel-view-contract.xml",
         "C:/suite/webappwriter/conf",
         "C:/suite/webappwriter/conf/temp");
   }

}
