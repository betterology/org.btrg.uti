package org.btrg.uti;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class InputStreamConverter_ {
	/*
	 * see http://ostermiller.org/convert_java_outputstream_inputstream.html if
	 * this method is too complex or does not work, there is an easier one that
	 * might consume more memory
	 * 
	 * I did not check this for logic or test it out
	 */
	private OutputStream outputStream;
	private InputStream inputStream;

	public InputStreamConverter_(OutputStream outputStream) {
		this.outputStream = outputStream;
		initialize();
	}

	private void initialize() {
		try {
			new Thread(new Runnable() {
				PipedInputStream in = new PipedInputStream();
				PipedOutputStream out = new PipedOutputStream(in);

				public void run() {
					outputStream = out;
				}
			}).start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
