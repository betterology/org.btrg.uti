package org.btrg.uti;

import java.io.File;
import java.io.IOException;

public class DeleteDirectory {

	public static void delete(File directory) throws IOException {
		if (directory.isDirectory()) {
			if (directory.list().length == 0) {
				directory.delete();
			} else {
				String files[] = directory.list();
				for (String temp : files) {
					File fileDelete = new File(directory, temp);
					delete(fileDelete);
				}
				if (directory.list().length == 0) {
					directory.delete();
				}
			}
		} else {
			directory.delete();
		}
	}
}
