package org.btrg.uti;

/**
 * This is one use class that should be deleted as soon as it is finished being
 * useful
 * 
 * @author petecarapetyan
 * 
 */

public class ConvertBToE {
	public static String convert(String b) {
		b = b.trim();
		String e = "0";
		if (b.equals("4097")) {
			e = "7";
		} else if (b.equals("1")) {
			e = "0";
		} else if (b.equals("136")) {
			e = "B";
		} else if (b.equals("131073")) {
			e = "f";
		} else if (b.equals("8193")) {
			e = "a";
		} else if (b.equals("262145")) {
			e = "g";
		} else if (b.equals("65")) {
			e = "2";
		} else if (b.equals("513")) {
			e = "5";
		} else if (b.equals("16385")) {
			e = "b";
		} else if (b.equals("16417")) {// invalid config on tag wtf
			e = "b";
		} else if (b.equals("2097153")) {
			e = "i";
		} else if (b.equals("524289")) {
			e = "d";
		} else if (b.equals("129")) {
			e = "3";
		} else if (b.equals("1025")) {
			e = "6";
		} else if (b.equals("32769")) {
			e = "c";
		} else if (b.equals("4194305")) {
			e = "j";
		} else if (b.equals("65537")) {
			e = "e";
		} else if (b.equals("2049")) {
			e = "7";
		} else if (b.equals("1048577")) {
			e = "h";
		} else if (b.equals("268435457")) {
			e = "0";
		} else if (b.equals("268443649")) {// root w children
			e = "a";
		} else if (b.equals("257")) {
			e = "4";
		} else if (b.equals("8388609")) {
			e = "8";
		} else {
			// RETURN HERE WHEN FIXING INT CONTENT TYPE
			// System.err.println("DID NOT PROCESs " + b);
		}
		return e;
	}
}
