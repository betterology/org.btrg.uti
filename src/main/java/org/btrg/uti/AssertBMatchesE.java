package org.btrg.uti;

/**
 * This is one use class that should be deleted as soon as it is finished being
 * useful
 * 
 * @author petecarapetyan
 * 
 */

public class AssertBMatchesE {
	public static boolean is(String b, String e) {
		String compareE = ConvertBToE.convert(b);
		if (e.equals(compareE)) {
			return true;
		} else {
			return false;
		}
	}
}
