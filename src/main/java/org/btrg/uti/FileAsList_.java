package org.btrg.uti;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class FileAsList_ {

	public static List<String> read(File file) {
		List<String> list = new ArrayList<String>();
		BufferedReader bufferedReader;
		String line = null;
		try {
			bufferedReader = new BufferedReader(new FileReader(file));
			line = bufferedReader.readLine();
			while (null != line) {
				list.add(line);
				line = bufferedReader.readLine();
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new IllegalArgumentException(e);
		}
		return list;
	}

	public static List<String> readShift6back(File file) {
		List<String> list = new ArrayList<String>();
		try {
			String fileContents = FileAsString_.read(file);
//			System.print.println("{" + fileContents + "}");
			StringTokenizer stk = new StringTokenizer(fileContents, "\n");
			while (stk.hasMoreElements()) {
				String newLine = (String) stk.nextElement();
				newLine = StringUtils_.shift6back(newLine);
//				System.print.println("[" + newLine + "]");
				list.add(newLine);
			}
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return list;
	}

	public static List<String> read(String filePath) {
		return read(new File((filePath)));
	}

}
