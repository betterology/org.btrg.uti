package org.btrg.uti;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class NioFileUtil {

	public static void append(String filePath, byte[] bytes) {
		File file = new File(filePath);
		try {
			Files.write(Paths.get(file.toURI()), bytes,
					StandardOpenOption.CREATE, StandardOpenOption.APPEND);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	public static void appendLine(String filePath, byte[] bytes) {
		byte[] one = "\n".getBytes();
		byte[] combined = new byte[one.length + bytes.length];
		System.arraycopy(one, 0, combined, 0, one.length);
		System.arraycopy(bytes, 0, combined, one.length, bytes.length);
		append(filePath, combined);
	}

}
