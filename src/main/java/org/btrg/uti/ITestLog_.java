package org.btrg.uti;

public interface ITestLog_ {
	public abstract void incompleteTest_(Object this_, String thisMethodName_);
	public abstract void incompleteCode_(Object this_, String thisMethodName_);

	public abstract void incompleteTestLowPriority(Object this_,
			String thisMethodName_);

	public abstract void incompleteTestInactivePart(Object this_,
			String thisMethodName_);

	public abstract void incompleteTestBecause_(Object this_,
			String thisMethodName_, String statement_);

	public abstract void incompleteCodeBecause_(Object this_, String thisMethodName_,
			String statement_);

	public abstract void testNotRequired_(Object this_, String thisMethodName_,
			String statement_);

	public abstract void toConsole_(String message_);

	public abstract void incompleteUiTest(Object this_, String thisMethodName_);
}
